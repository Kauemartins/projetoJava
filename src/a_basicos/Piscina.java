package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe as três dimensões de uma piscina
 * (largura, comprimento, profundidade) e retorne a sua capacidade
 * em litros.
 */
public class Piscina {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		
		System.out.println("Digite a largura da Piscina");
		double largura = scanner.nextDouble();
		
		System.out.println("Digite o comprimento da Piscina");
		double comprimento = scanner.nextDouble();
		
		System.out.println("Digite a profundidade da Piscina");
		double profundidade = scanner.nextDouble();
		
		double capacidade = (largura * comprimento * profundidade);
		
		System.out.println("A capacidade da Piscina é:" + capacidade + "LT");
	}
}
