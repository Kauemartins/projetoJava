package a_basicos;

/**
 * Crie um programa que imprima todas as cartas do baralho.
 * 
 * Exemplo: 
 * 
 * Ás de Ouros
 * Ás de Espadas
 * Ás de Copas
 * Ás de Paus
 * Dois de Ouros
 * ...
 * 
 */
public class Baralho {
	public static void main(String[] args) {
		String[] nipes  = {"Ouros", "Espadas", "Copas", "Paus", "Ouros"};
		String[] cartas = {"Ás", "Dois", "Três", "Quatro", "Cinco", "Seis", "Sete", "Oito", "Nove", "Dez", "Rei", "Rainha", "Valete"};
	
		for(String carta : cartas) {
			 for(String nipe : nipes) {
				 System.out.println(carta + " de " + nipe);
			 }
		 }
	
	}
}
