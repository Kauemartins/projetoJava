package a_basicos;

import java.util.Scanner;

import com.sun.org.apache.xml.internal.security.utils.SignerOutputStream;

/**
 * Crie um programa que recebe um número do usuário
 * e imprime a fatorial desse mesmo número.
 * 
 */
public class Fatorial {
	public static void main(String args[]) {
	
		Scanner scanner = new Scanner(System.in);
		System.out.println("Digite um Valor");
		
		int valor = scanner.nextInt();
		int Fatorial = valor;
		
		while (valor > 1) {
			Fatorial =  Fatorial * (valor - 1);
			valor --;
		}
		
		System.out.println("O fatorial desse numero é: " + Fatorial);
		
	}
}
