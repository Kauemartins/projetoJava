package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe uma palavra do terminal e determina
 * se ela é um palíndromo.
 * 
 * Exs: 
 * 
 * input: ovo
 * output: A palavra ovo é um palíndromo
 * 
 * input: jose
 * output: A palavra jose não é um palíndromo 
 */
public class Palindromo {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Digite uma palavra");
		String palavra = scanner.nextLine();
		String palavraInvertida = new StringBuffer(palavra).reverse().toString();
		
		if (palavraInvertida.equals(palavra)) {
			System.out.println("É palindromo");
		}else {
			System.out.println("Não é Palindromo");
		}
		
	}
}
