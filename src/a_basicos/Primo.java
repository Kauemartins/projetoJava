package a_basicos;

/**
 * Crie um programa que imprima todos os números
 * primos de 1 à 100
 */
public class Primo {
	public static void main(String[] args) {
		
		
		for(int i = 2; i <= 100; i++) {
			if(verificarPrimo(i))
				System.out.println(i + " é primo");
		}
	}
		private static boolean verificarPrimo(int numero) {
				for (int j = 2; j < numero; j++) {
					if (numero % j == 0)
						return false;
				}
				return true;
		}
		
}
